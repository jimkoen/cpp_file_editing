//
// Created by Lucca Jiménez Könings on 2019-04-03.
//

#include "File.h"

FileIO::PlainTextFile::PlainTextFile(std::string pathToFile) : sourceFileStream{pathToFile,
                                                                                std::ios::in | std::ios::binary},
                                                               pathToFile{pathToFile} {

}

void FileIO::PlainTextFile::read() {
    if (sourceFileStream) {
        std::ostringstream contents;
        contents << sourceFileStream.rdbuf();
        sourceFileStream.close();
        buffer = contents.str();
        this->parseToLineBuffer();
        return;
    }
    throw std::runtime_error("Can't read file \"" + pathToFile + "\"\nProgram is aborting.");
}

void FileIO::PlainTextFile::write(std::string pathName) {

}


std::string FileIO::PlainTextFile::getLine(size_t row) const {
    if (lineBuffer.empty()) {
        throw std::logic_error(
                "Can't return line " + std::to_string(row) + " of file " + pathToFile + "\nLine buffer is empty");
    } else {
        try {
            lineBuffer.at(row);
        } catch (std::out_of_range &e) {
            throw std::logic_error("Can't return line " + std::to_string(row) + " of file " + pathToFile +
                                   "\nLine does not exist.\nSize of lineBuffer: " + std::to_string(lineBuffer.size()));
        }
    }
}

std::string FileIO::PlainTextFile::getLines(size_t beginRow, size_t endRow) const {
    std::string lines{};
    for (size_t i{beginRow}; i < endRow; i++) {
        lines += (lineBuffer.at(i) + '\n');
    }
    return (lines += lineBuffer.at(endRow));
}

std::string FileIO::PlainTextFile::getBuffer() const {
    return buffer;
}

void FileIO::PlainTextFile::parseToLineBuffer() {
    size_t lineStartPosition{0};
    for (size_t i{0}; i < buffer.length(); i++) {
        if (/*buffer.at(i) == '\r' ||*/ buffer.at(i) == '\n') {
            if (buffer.at(i - 1) == '\r') { // clear crlf
                lineBuffer.push_back(buffer.substr(lineStartPosition, (i - lineStartPosition) -
                                                                      1)); // lineBuffer.at(i) = '\n'; lineBuffer.at(i-1) = '\r', crlf,push back everything before '\r'
                lineStartPosition = i + 1;
            } else {// clear cl or rf
                lineBuffer.push_back(
                        buffer.substr(lineStartPosition, i - lineStartPosition)); // lineBuffer.at(i) = '\n' | '\r'
                lineStartPosition = i + 1;
            }
        }
    }
}

size_t FileIO::PlainTextFile::getSizeOfLineBuffer() {
    return lineBuffer.size();
}

std::string FileIO::PlainTextFile::getLineBufferContentFormatted() {
    std::string bufferCopy{};
    for (size_t i{}; i < lineBuffer.size(); i++) {
        bufferCopy += (lineBuffer.at(i) + '\n');
    }
    return bufferCopy;
}