#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <stdexcept>
#include "File.h"

int main() {
    FileIO::PlainTextFile test{"/tmp/helloworld.txt"};
    test.read();
    std::cout << test.getBuffer();
    return 0;
}