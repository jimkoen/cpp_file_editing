//
// Created by Lucca Jiménez Könings on 2019-04-03.
//

#ifndef FILEIO_FILE_H
#define FILEIO_FILE_H

#include <string>
#include <vector>
#include <sstream>
#include <stdexcept>
#include <fstream>

namespace FileIO {
    class PlainTextFile {
    public:
        PlainTextFile(std::string pathToFile);

        void read();

        void write(std::string pathName);

        std::string getLine(size_t row) const;

        std::string getLines(size_t beginRow, size_t endRow) const;

        std::string getBuffer() const;

        std::string getLineBufferContentFormatted();

        size_t getSizeOfLineBuffer();

    private:
        std::string pathToFile{};
        std::string buffer{};
        std::vector<std::string> lineBuffer{};
        std::ifstream sourceFileStream;

        void parseToLineBuffer();
    };
}


#endif //FILEIO_FILE_H
