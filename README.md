# Header FileIO
This header provides classes and functions for easy manipulation of file
contents, primarily plaintext based files. Files are thought as unique
rescources, that are "owned" by one or more instances of FileIO file objects.

## Class PlainTextFile
Provides acess to plaintext based files, as well as reading and writing
capabilities. 

### Constructor ```PlainTextFile(std::string pathToFile)```
An instance of the PlainTextClass must be initialized with a path,
the reasoning behind this being that after initialization the file
object "owns" this file in context of the application, so that no
other object can interfere with reading/writing/editing operations of
the file.

## Public Member Functions of ```class plainTextFile```

### ```void read()```
Reads the contents of the file specified in ```pathToFile``` as passed to the
constructor. If the file can't be read an exception of type 
```std::runtime_error``` is thrown.

### ```void write()```
unimplemented

### ```std::string getLine(size_t row)```
Returns a string containing entire contents of the line specified with
row, row being the number of the line to extract from
(think of it as text editor). 
**Indexing of lines starts at 0.**
**Lines are returned without line break
characters ('\r', '\n', "\r\n").**

### ```std::string getLines(size_t beginRow, size_t endRow)```
Returns a string containing all contents of the lines between beginRow and
endRow. 
**Indexing of lines starts at 0**
**'\n' character is appended between the lines. The last line does _not_
end with a '\n'**

### ```std::string getBuffer```
Returns a string containing entire content extracted from the file since last
read or write;

### ```std::string getLineBufferContentFormatted()```
Returns a copy of the entire ```lineBuffer``` which holds the contents of
all the lines in a file inside an ```std::vector<std::vector<std::string>>```.
Appends '\n' to every line **including the last one** (string will end on '\n').

### ```size_t getSizeOfLineBuffer```
Returns the size of the lineBuffer (amount of lines in file).
**Indexing starts at 1**